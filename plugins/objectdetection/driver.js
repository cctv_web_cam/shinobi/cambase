const fork = require('child_process').fork;
const fs = require('fs').promises;
const { forkChildProcess } = require('./libs/utils/src');

async function loadConfig() {
    return JSON.parse(await fs.readFile('./tests/config/conf.json'));
}

async function loadMessage() {
    return JSON.parse(await fs.readFile('./tests/data/personDetection.json'));
}

/** main execution thread */

loadConfig().then((config) => {

    forkChildProcess(config, `${__dirname}/notify.js`)
        .then((child) => {
            console.log(`Forked and initialised child process => process.id := ${child.pid}`);

            loadMessage()
                .then((message) => {
                    child.send(message, () => {
                        console.log('Parent despatched test message to child');
                    });
                })
                .catch((error) => {
                    console.error(`Failed to load message := ${error}`);
                });

            wait(1000).then(() => {

                loadMessage()
                    .then((message) => {
                        child.send(message, () => {
                            console.log('Parent despatched test message to child');
                        });
                    })
                    .catch((error) => {
                        console.error(`Failed to load message := ${error}`);
                    });

                    wait(1000).then(()=> {
                        console.log('Killing child process with SIGTERM');
                        child.kill('SIGTERM');
                    });
            });
        })
        .catch((error) => {
            console.error(`Moose ${error}`);
        });
});


async function wait(seconds) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        },
            seconds);
    });
}
