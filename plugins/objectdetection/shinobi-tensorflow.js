//
// Shinobi - ObjectDetection Tensorflow plugin based on:
// 	Shinobi - Tensorflow Plugin
// 	Copyright (C) 2016-2025 Moe Alam, moeiscool
//
// Added functionality to send notification to MQTT broker using connection
// parameters in plugin config. Messages sent on queue must pass the filter
// conditions defined in plugin config:
// - >= minimum confidence threshold
// - predicted object class should match set of classes defined by the filter
//
// Enhancements:
// - Read the filter configuration defined in shinobi's filter detection
//   I am new to the source code, for now using plugin config....
//
// Copyright (C) 2020 Simon Pears


// Base Init >>
var config = require('./conf.json');

var s
try {
    s = require('../pluginBase.js')(__dirname, config);
} catch (err) {
    console.log(`Error encountered loading ../pluginBase.js => ${err}`);
    try {
        s = require('./pluginBase.js')(__dirname, config);
    } catch (err) {
        console.error(err);
        //return;
        // return console.log(config.plug, 'Plugin start has failed. pluginBase.js was not found.')
    }
}
// Base Init />>


const ObjectDetectors = require('./ObjectDetectors.js')(config);

const filterObj = require('./filter.js')(config.objectDetection.minConfidence, config.objectDetection.objectClasses);
console.log('Acquired filter object...');

const { forkChildProcess } = require('./libs/utils/src');
const childProcessPromise = forkChildProcess(config, `${__dirname}/notify.js`);


const ENCODING = 'base64';


/**
 * Override pluginBase detectObject. 
 * Despatches tensorflow model predictions onto message queue
 * 
 * ObjectDetectors class filters matches according to confidence threshold value
 * and object class.
 */
s.detectObject = function (buffer, d, tx, frameLocation) {

    new ObjectDetectors(buffer).process().then((resp) => {

        let filtered = false;

        var results = resp.data;

        if (results[0]) {
            var mats = []
            results.forEach(function (v) {
                mats.push({
                    x: v.bbox[0],
                    y: v.bbox[1],
                    width: v.bbox[2],
                    height: v.bbox[3],
                    tag: v.class,
                    confidence: v.score,
                })

                if (!filtered) {
                    filtered = filterObj.filter(v.class, v.score);
                }
            });

            var isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1'
            var width = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
            var height = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)

            tx({
                f: 'trigger',
                id: d.id,
                ke: d.ke,
                details: {
                    plug: config.plug,
                    name: 'Tensorflow',
                    reason: 'object',
                    matrices: mats,
                    imgHeight: width,
                    imgWidth: height,
                    time: resp.time
                }
            });

            // if the detection passed the filter rules then dispatch onto the message queue
            if (filtered) {
                const message = {
                    group: d.ke,
                    time: resp.time,
                    monitorId: d.id,
                    plug: config.plug,
                    details: {
                        plug: config.plug,
                        name: 'Tensorflow',
                        reason: 'object',
                        matrices: mats,
                        img: buffer.toString(ENCODING),
                        imgHeight: width,
                        imgWidth: height,
                        time: resp.time
                    }
                }

                childProcessPromise
                    .then((childProcess) => {
                        childProcess.send(message);
                    })
                    .catch((error) => {
                        console.error(`${error}`);
                    });
            }
        }
    });
}

