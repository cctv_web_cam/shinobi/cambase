#!/usr/bin/env bash

##
#
# Plugin installation script
#
# A package.json file has been added to facilitate managing the
# installation of the plugin
#
# @tensorflow/tfjs-node-gpu is listed as an optional dependency
# in package.json
#
# This will extract libtensorflow precompiled library and headers
# compatible with Intel core2. Compiled with Ubuntu Server 20.04
#
# For information on howto compile for your architecture please
# refer to =>
# https://gitlab.com/dcs3spp/myfiles/-/blob/master/COMPILING_LIBTENSORFLOW.md
#
##


. ./functions.sh


DIR=`dirname $0`
echo "Running in directory => ${DIR}"


echo "Do you want to install the plugin with GPU support? "
echo "You can run this installer again to change it."
echo "(y)es or (N)o"


read nodejsinstall


echo "Removing existing @tensorflow/tfjs-node packages..."
rm -rf node_modules/@tensorflow/tfjs-node
rm -rf node_modules/@tensorflow/tfjs-node-gpu


GPU_INSTALL="0"
if [ "$nodejsinstall" = "y" ] || [ "$nodejsinstall" = "Y" ]; then
    GPU_INSTALL="1"
    yarn install --audit
else 
    yarn install --audit --ignore-optional
fi
yarn audit

if [ ! -e "./conf.json" ]; then
    echo "Creating conf.json"
    sudo cp conf.sample.json conf.json
else
    echo "conf.json already exists..."
fi


echo "Do you have an Intel core2 CPU?"
echo "(y)es or (N)o"

read core2libinstall

if [ "$core2libinstall" = "y" ] || [ "$core2libinstall" = "Y" ]; then
    echo "Removing existing tensorflow precompiled CPU library and header files"
    rm -rf $DIR/node_modules/@tensorflow/tfjs-node/deps/lib
    rm -rf $DIR/node_modules/@tensorflow/tfjs-node/deps/include

    echo "Extracting Intel Core2 compatible tensorflow CPU library"
    tar zxf "$DIR/third-party/libs/tfjs-node/core2/libtensorflow-1.15.3.tar.gz" -C "$DIR/node_modules/@tensorflow/tfjs-node/deps/"
fi


echo "Adding Random Plugin Key to Main Configuration"
tfjsBuildVal="cpu"
if [ "$GPU_INSTALL" = "1" ]; then
    tfjsBuildVal="gpu"
fi

FILE="$DIR/../../tools/modifyConfigurationForPlugin.js"
if [ -f "$FILE" ]; then
    node $DIR/../../tools/modifyConfigurationForPlugin.js objectdetection key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}') tfjsBuild=$tfjsBuildVal
    echo "Installation completed!"
    exit 0
else
    echo "$FILE could not be located for writing plugin key to base Shinobi configuration"
    exit 1
fi

