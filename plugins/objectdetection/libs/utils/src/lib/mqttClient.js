'use strict';


const fs = require('fs');
const mqtt = require('async-mqtt');

const { parentLogger } = require('./logger');
const log = parentLogger.child({module: 'mqttClient'});


/** @class Class to allow publishing mqtt messages */
class MQTTClient {


    /**
     * Create an MQTT client to publish topics 
     *
     * @constructor
     * 
     * @param {string} url - mqtts connection url 
     * @param {string} caFilePath - path to CA file
     * @param {boolean} rejectUnauthorised - true if unauthorised Certificate Authorities should be rejected, defaults to false
     */
    constructor(url, caFilePath, rejectUnauth = true, connectTimeout = 30000) {
        if (caFilePath === undefined) {
            throw new Error('CA certificate must be specified');
        }

        log.debug(`MQTTClient reading ca file from ${caFilePath}`);
        this.url = url;
        this.options = {
            ca: fs.readFileSync(caFilePath),
            rejectUnauthorized: rejectUnauth,
            connectTimeout: connectTimeout
        };
    }


    /**
     * Publish a message to the mqtt broker for a given topic
     * Disposes the client connection upon send success or failure.
     * @param {string} topic - topic to publish to 
     * @param {Object} message - message to publish
     * 
     * @throws {Error} Upon failure to connect to client
     * @throws {Error} When fails to publish message
     * @throws {Error} When fails to disconnect from client after publishing.
     */
    async publish(topic, message) {

        let client = undefined;

        try {
            log.debug(`MQTTClient awaiting connection to ${this.url} for publishing...`);
            client = await mqtt.connectAsync(this.url, this.options, false);
        }
        catch(error) {
            throw new Error(error);
        }

        try {
            log.debug('MQTTClient acquired connection, publishing...');
            await client.publish(topic, message);
            log.debug(`Published message to topic := ${topic}`);
        } catch (error) {
            console.error(`Failed to publish message to topic := ${topic}`);
            throw error;
        }
        finally {
            if (client) {
                try {
                    log.debug('MQTTClient closing connection');
                    await client.end();
                }
                catch (error) {
                    throw error;
                }
            }
        }
    }
}


/** 
 * Factory method that creates a class to publish an MQTT message for a given topic
 * 
 * @param {string} url - Minimum confidence rating score to satisfy filter
 * @param {string} caFilePath - Comma delimited list of classes recognised by filter 
 * @param {boolean} rejectUnauthorised - True to reject unauthorised CAs
 * @param {number} connectTimeout - Timeout in milliseconds for trying to connect
 * 
 * @returns {MQTTClient} - Instance of MQTTClient class
 */
module.exports = (url, caFilePath, rejectUnauthorised=true, connectTimeout=3000) => {
    return new MQTTClient(url, caFilePath, rejectUnauthorised, connectTimeout);
};
