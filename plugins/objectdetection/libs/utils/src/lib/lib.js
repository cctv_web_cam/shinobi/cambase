'use strict';


const { forkChildProcess, Process } = require('./process');
const { parentLogger } = require('./logger');

module.exports =
{ 
    forkChildProcess,
    parentLogger,
    Process 
}
