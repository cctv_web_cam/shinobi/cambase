'use strict';


const fork = require('child_process').fork;

const { parentLogger } = require('./logger');
const log = parentLogger.child({ module: 'process' });

const mqttsConnect = require('./mqttClient.js');
const { ValidateProcess } = require('./validation.js');
const { Timer } = require('./timer.js');



/** Class to manage process arguments */
class ProcessArgs {

    /**
     * @constructor
     * Constructor that initilises process arguments as follows:
     * mqttsUrl = ''
     * cafile = ''
     * timeoutPeriod = 0
     * rejectUnauthorised = true
     */
    constructor(args) {

        /**
         * {string[]} Process arguments
         */
        this.args = args;


        this.mqttsUrl = '';
        this.cafile = '';
        this.timeoutPeriod = 0;
        this.rejectUnauthorised = true;
    }


    /**
     * Initialise process arguments for mqttsUrl, cafile, timeoutPeriod and rejectUnauthorised
     * Process arguments are initialised from process.argv
     */
    async init() {

        let validatedArgs = undefined;
        try {
            const validation = new ValidateProcess(this.args);
            validatedArgs = await validation.initArgs();
            this.mqttsUrl = validatedArgs.mqttsUrl;
            this.cafile = validatedArgs.cafile;
            this.timeoutPeriod = validatedArgs.timeoutPeriod;
            this.rejectUnauthorised = validatedArgs.rejectUnauthorised;

            log.debug(`Accepted arguments => url := ${this.mqttsUrl} :: caFile := ${this.cafile} :: rejectUnauthorised := ${this.rejectUnauthorised} :: timeoutPeriod := ${this.timeoutPeriod}`);
        }
        catch (err) {
            log.error(`${err}`);
            throw new Error(err);
        }

        return validatedArgs;
    }
}


/** Class to manage this child process for sending messages to MQTT */
class Process {


    /**
     * Constructor that initialises process arguments.
     * Initialses an empty dictionary of timers to track messages dispatched for each camera.
     * @constructor
     * @param {Array.<string>} args - Process arguments include mqttsUrl, cafile, timeoutPeriod, rejectUnauthorised
     */
    constructor(argsv) {

        /**
         * @typedef ProcessArgs
         * @type {object}
         * @property {string} mqttsUrl - Url for mqtt broker that uses protocol mqtts.
         * @property {string} cafile - Path to certificate authority file for connecting to mqtt broker.
         * @property {number} timeoutPeriod - The timeout period in milliseconds.
         * @property {boolean} rejectUnauthorised - True if unauthorised ceritifcate authorities should be reject when using mqtts.
         */
        this.args = new ProcessArgs(argsv);

        /**
         * Dictionary of timers running, identified by groupId+monitorId
         * @type{{id: string, Timer}} Dictionary of timer instances identified by groupId+monitorId
         */
        this.timers = {};

        /**
         * Client for MQTT queue
         */
        this.mqttClient = undefined;
    }


    /**
     * Dispose of process resources by cancelling the timers triggered per camera
     */
    dispose() {
        Object.keys(this.timers).forEach(key => {
            this.timers[key].cancel();
        });
        log.debug('Process dispose handler complete');
    }


    /**
     * Error handler
     * @param {Error} err - Error instance 
     */
    errorHandler(err) {
        if (err) {
            console.error(err);
        }
    }


    /**
     * Initialise and validate the process arguments.
     * Despatches an 'INITIALISATION' message to the parent in completion
     * { type: 'INITIALISATION', error: undefined, success: false }
     * If an error is encountered while initialising despatches.....
     * { type: 'INITIALISATION', error: err, success: false }
     */
    async init() {

        try {
            await this.args.init();

            this.mqttClient = mqttsConnect(this.args.mqttsUrl, this.args.cafile, this.args.rejectUnauthorised);

            // send a message to parent as notification ready
            process.send({ type: 'INITIALISATION', error: undefined, success: true });
        }
        catch (err) {
            log.error(`${err}`);
            process.send({ type: 'INITIALISATION', error: err, success: false });
        }
    }

    getMqttsUrl() {
        return this.args.mqttsUrl;

    }
    /**
     * Handle a message sent by parent process (shinobi-tensorflow.js)
     * @param {Message} data The message payload
     */
    async messageHandler(data) {

        const key = data.group + data.monitorId;
        if (this.timers[key]) {
            log.debug(`Previously encountered a detection for camera ${data.group}\\${data.monitorId}`);
            if (!this.timers[key].isRunning()) {
                log.debug('Sending notification of detection via MQTT for this camera since timer is not currently running');
                
                try {
                    await this._sendDetection(data);
                    this.timers[key].run();
                }
                catch(error) {
                    log.error(error);
                    throw error;
                }
            }
            else {
                log.debug('Skipping notification of detection for this camera, timer is currently running');
            }
        }
        else {
            log.debug('Sending notification of detection via MQTT for the first time for this camera');

            try {
                await this._sendDetection(data);
                const timer = new Timer(
                    data.monitorId,
                    data.group,
                    this.args.timeoutPeriod
                );
                this.timers[key] = timer;
                timer.run();
            }
            catch (error) {
                log.error(error);
                throw error;
            }
        }
    }

    /**
     * Dispatch message to MQTT asynchronously
     * @param {Object} message - Message payload
     * @returns {Promise} Promise resolves when the publish operation
     * has been performed. The promise rejects when error encountered
     * during publishing or the mqtt client has not been initialised.
     */
    async _sendDetection(message) {

        return new Promise((resolve, reject) => {
            if (this.mqttClient) {
                const topic = `shinobi/${message.group}/${message.monitorId}/trigger`;

                this.mqttClient.publish(
                    `${topic}`,
                    JSON.stringify(message)
                )
                    .then(() => {
                        resolve();
                    })
                    .catch((error) => {
                        reject(error);
                    });
            }
            else {
                reject('MQTT client not initialised');
            }
        });
    }
}


/**
 * Fork the child process and wait for it to initialise.
 * Registers listeners for exit, error, message and uncaughtException events.
 * @param {Object} config - Object representing process configuration
 * @param {Object} config.mqtts - Object representing args for the MQTT broker 
 * @param {string} config.mqttsUrl - The url for the MQTT broker in format
 * expected by mqtts.js
 * @param {string} config.cafile - Absolute path to certificate authority file
 * @param {boolean} config.rejectUnauthorised - Should untrusted certificates
 * authorities be rejected. If this option is 'false' then could be subjected
 * to 'man in the middle' attacks
 * @param {Object} config.objectDetection - Object detection configurarion args
 * @param {number} config.objectDetection.timeoutPeriod - Number of milliseconds 
 * to wait between sending detection notifications on the queue
 * @param {string} scriptPath - Absolute path to script for child execution body.
 * @returns {Promise} - Resolves with child process handle once a message has 
 * been received from client for successful initialisation. If an 
 * INITIALISATION message is received with an error property then the promise
 * is rejected.
 */
async function forkChildProcess(config, scriptPath) {

    return new Promise((resolve, reject) => {
        try {
            const args = validateOptions(config);
            const child = fork(scriptPath, args);

            child.on('error', (err) => {
                reject(err);
            });

            child.on('exit', (code, signal) => {
                if (code) {
                    if (code === 0) {
                        resolve(code);
                    }
                    else {
                        reject(`Child exiting with code ${code}`);
                    }
                }
                else if (signal) {
                    resolve(signal);
                }
            });

            child.on('uncaughtException', (err) => {
                resolve(err);
            });

            child.on('message', (childMessage) => {
                if (childMessage && !childMessage.type) {
                    return;
                }

                switch (childMessage.type) {
                    case 'INITIALISATION': {
                        if (childMessage.success) {
                            resolve(child);
                        }
                        else {
                            reject(childMessage.error);
                        }
                        break;
                    }
                }
            });
        }
        catch (error) {
            reject(error);
        }
    });
}



/**
 * Validate args object for inclusion of expected process configuration options
 * @param {Object} args - Object representing process configuration
 * @param {Object} args.mqtts - Object representing args for the MQTT broker 
 * @param {string} args.mqttsUrl - The url for the MQTT broker in format
 * expected by mqtts.js
 * @param {string} args.cafile - Absolute path to certificate authority file
 * @param {boolean} args.rejectUnauthorised - Should untrusted certificates
 * authorities be rejected. If this option is 'false' then could be subjected
 * to 'man in the middle' attacks
 * @param {Object} args.objectDetection - Object representing object detection args
 * @param {number} args.objectDetection.timeoutPeriod - Number of milliseconds 
 * to wait between sending detection notifications on the queue
 * @returns {Array.<string>} - Process arguments arranged as an array of strings
 * according to [Process constructor]{@link Process#constructor}
 * @throws {Error} - When a required argument is missing
 * @see Process
 */
function validateOptions(args) {

    if (!args) {
        throw new Error('Arguments are empty');
    }
    else if (!args.mqtt) {
        throw new Error('Arguments are missing the mqtt property');
    }
    else if (!args.objectDetection) {
        throw new Error('Arguments are missing the object detection property');
    }
    else { // check required properties are exist in options
        if (!args.mqtt.mqttsUrl) {
            throw new Error('Missing mqtt.mqttsUrl argument');
        }
        else if (!args.mqtt.cafile) {
            throw new Error('Missing mqtt.cafile argument');
        }
        else if (!args.mqtt.rejectUnauthorised) {
            throw new Error('Missing mqtt.rejectUnauthorised argument');
        }
        else if (!args.objectDetection.timeoutPeriod) {
            throw new Error('Missing objectDetection.timeoutPeriod argument');
        }
    }

    return [
        args.mqtt.mqttsUrl,
        args.mqtt.cafile,
        args.mqtt.rejectUnauthorised,
        args.objectDetection.timeoutPeriod
    ];
}





module.exports = {
    Process,
    forkChildProcess
}
