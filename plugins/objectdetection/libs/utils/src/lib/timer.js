'use strict'


const { parentLogger } = require('./logger');
const log = parentLogger.child({module: 'timer'})


/** 
 * Class to manage timeout state for a shinobi camera group and monitor 
 * @class 
 */
class Timer {

    /**
     * Constructor to manage timeout
     * @constructor
     * 
     * @param {string} monitorId - Camera monitor Id
     * @param {string} groupId - Camera group Id
     * @param {number} timeoutPeriod - Timeout period in milliseconds
     */
    constructor(monitorId, groupId, timeoutPeriod) {
        this.monitorId = monitorId;
        this.groupId = groupId;
        this.running = false;
        this.timeoutObj = undefined;
        this.timeoutPeriod = timeoutPeriod;

        this.timeoutComplete = () => {
            log.debug(`Timer complete for ${this.groupId}/${this.monitorId}`);
            this.running = false;
            this.timeoutObj = undefined;
        }
    }


    /** Start a timeout period */
    run() {
        if (!this.running) {
            log.debug(`Starting timeout period for ${this.timeoutPeriod} milliseconds`);
            this.timeoutObj = setTimeout(this.timeoutComplete, this.timeoutPeriod);
            this.running = true;
        }
    }

    /** Clear timeout */
    cancel() {
        if (this.timeoutObj) {
            log.debug(`Cancelling timeout`);
            clearTimeout(this.timeoutObj);
            this.running = false;
            this.timeoutObj = undefined;
            log.debug('Timeout cancelled');
        }
    }

    /**
     * Return timeout status
     * @returns {bool} true if running, otherwise false
     */
    isRunning() {
        return this.running;
    }
}

module.exports = {
    Timer
}