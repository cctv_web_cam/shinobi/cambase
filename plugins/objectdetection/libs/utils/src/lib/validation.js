'use strict';


const fs = require('fs').promises;

const { parentLogger } = require('./logger');
const log = parentLogger.child({module: 'validation'});




class ValidateProcess {

    /**
     * 
     * @param {Array.<string>} args - List of process arguments from process.argv 
     */
    constructor(args) {
        this.args = args;
    }


    /**
     * Validate and return an object representation of the process arguments
     * @returns {Args} - Returns an object structure of validated arguments
     * @throws {Error} -  mqttsUrl, caFile, rejectUnauthorised and timeoutPeriod 
     *     arguments must be supplied in correct format.
     */
    async initArgs() {
        const valid = await this._validateArgs();
        if (!valid) {
            throw new Error(`Invalid arguments encountered for ${__filename}`);
        }

        return this._getArgs();
    }


    /**
     * Validate arguments for this process
     * @returns {boolean} True if all args are present and of correct types, otherwise false
     */
    async _validateArgs() {
        const CaFileIndx = 3;
        const ExpectedTotalArgs = 6;
        const RejectUnauthorisedIndx = 4;
        const TimeoutIndx = 5;
        
        // validate total arguments
        if (this.args.length !== ExpectedTotalArgs) {
            log.error(`Total expected arguments should be ${ExpectedTotalArgs}`);
            return false;
        }

        // validate timeout period
        if (isNaN(this.args[TimeoutIndx])) {
            log.error(`Timeout period should be a number`);
            return false;
        }

        // validate legal boolean values for rejectUnauthorised
        const rejectUnauthorised = this.args[RejectUnauthorisedIndx].toLowerCase();
        if (rejectUnauthorised !== 'false' && rejectUnauthorised !== 'true') {
            log.error(`rejectUnauthorised should be true or false, value supplied is ${rejectUnauthorised}`);
            return false;
        }

        // validate caFile exists
        try {
            await fs.access(this.args[CaFileIndx]);
        }
        catch(err) {
            log.error(`Failed to access certificate authority file ${this.args[CaFileIndx]}`);
            return false;
        }

        return true;
    }


    /**
     * Retrieve the arguments for this process
     * @returns {Args} Returns an object structure encapsulating the arguments for this process
     */
    _getArgs() {
        return {
            mqttsUrl: this.args[2],
            cafile: this.args[3],
            rejectUnauthorised: (this.args[4].toLowerCase() == 'true'),
            timeoutPeriod: parseInt(this.args[5])
        }
    }
}

module.exports = {
    ValidateProcess
}