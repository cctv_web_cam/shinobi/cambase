'use strict'

const pino = require('pino');
const log = pino({name: 'parent', level: 'debug'}, pino.destination(1));

module.exports = {
    parentLogger: log
}