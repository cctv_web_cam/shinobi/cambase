'use strict';


const { forkChildProcess, parentLogger, Process } = require('./lib/lib.js');


module.exports = {
    forkChildProcess,
    parentLogger,
    Process
}
