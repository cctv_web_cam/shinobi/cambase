'use strict';

/** 
 * notify.js
 * 
 * Child process that manages notification of object detections on message queue
 *
 * Arguments:
 * - mqttsUrl               The url to MQTTS broker
 * - cafile                 The path to the caFile for MQTTS
 * - rejectUnauthorised     True if unauthorised certificate authorities should be rejected
 * - timeoutPeriod          The timeout period in milliseconds between accepting messages for queue
 *                          for a specific groupid/monitorId camera.
 *
 * Message:                 Message payload, contains groupId and monitorId for source camera
 *                          {
 *                              group: 'myGroup',
 *                              time: 2020-04-24T13:49:16+00:00,
 *                              monitorId: 'myMonitorId',
 *                              plug: TensorFlow-WithFiltering-And-MQTT,
 *                              details: {
 *                                  plug: TensorFlow-WithFiltering-And-MQTT,
 *                                  name: 'Tensorflow',
 *                                  reason: 'object',
 *                                  matrices: [{
 *                                      "x": 2.313079833984375,
 *                                      "y": 1.0182666778564453,
 *                                      "width": 373.25050354003906,
 *                                      "height": 476.9341278076172,
 *                                      "tag": "person",
 *                                      "confidence": 0.7375929355621338
 *                                  }],
 *                                  img: 'base64',
 *                                  imgHeight: 64,
 *                                  imgWidth: 48,
 *                                  time: 2020-04-24T13:49:16+00:00
 *                              }
 *                          }
 * 
 */

const { parentLogger, Process } = require('./libs/utils/src');
const log = parentLogger.child({module: 'process'});


/* ====== Functions ======================================================= */

/**
 * Dipose process resources by cancelling timeout handlers
 * @param {Process} child - Child process instance 
 */
function dispose(child) {

    if (child) {
        child.dispose();
    }
}


/**
 * Entrypoint to initialise and validate process arguments
 * Registers event listeners for error, exit and message events.
 * @returns {Process} An instance of the main process to connect to
 * @throws {Error} If arguments invalid
 */
async function main() {

    const me = new Process(process.argv);

    /* ====== Process Event Handlers ======================================== */

    /**
     * Message handler that listens for data from parent process
     * @event message = Triggered when the process receives data
     * @param {Object} data - The message to dispatch onto the queue, e.g.
     * {
            group: d.ke,
            time: resp.time,
            monitorId: d.id,
            plug: config.plug,
            details: {
                plug: config.plug,
                name: 'Tensorflow',
                reason: 'object',
                matrices: mats,
                img: buffer.toString(ENCODING),
                imgHeight: width,
                imgWidth: height,
                time: resp.time
            }
        }
    *
    */
    process.on('message', (data) => {
        if (me) {
            me.messageHandler(data);
        }
    });


    /**
     * Error handler to display Error instance on error stream
     * @event error - Triggered when the process encounters an error
     * @param {Error} err - Error 
     */
    process.on('error', (err) => {
        if (me) {
            me.errorHandler(err);
        }
    });


    /**
     * Signal handlers 
     */
    process.on('SIGTERM', (signal, code) => {
        signalExit(code, me);
    });

    process.on('SIGINT', (signal, code) => {
        signalExit(code, me);
    });

    /* ==================================================================== */

    // initialise and validate args
    await me.init();
}



/* ====== Functions ======================================================= */


/**
 * Reset the terminal, dispose of resources and exit with code 128+exitCode
 * @param {number} exitCode - Exit code
 * @param {Process} me - Process resource manager 
 */
function signalExit(exitCode, me) {
    //process.stdout.write('\x1Bc');
    dispose(me);
    process.exit(128 + exitCode);
}

/* ====== End Functions =================================================== */



/* ====== Main execution body ============================================= */

main()
    .then(() => {
        log.debug('notify.js process finished initialising');
    })
    .catch((error) => {
        log.error(`notify.js process failed to initialise := ${error}`);
    });

/* ====== End Main execution body ========================================= */
