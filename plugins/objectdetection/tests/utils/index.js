'use strict'


/**
 * Helper utilities for testing
 */

const fs = require('fs').promises;




/**
 * Promise that resolves when a message has been received a specific number
 * times.
 * @param {*} mqttClient - MQTT.js client
 * @param {*} expectedMessage - Message to count occurrences for
 * @param {string} expectedTopic - The topic message has been published for
 * @param {number} maxCount - Max count value before promise resolves 
 */
async function countMessageReceivedAsync(mqttClient, expectedMessage, expectedTopic, maxCount) {
    return new Promise((resolve, reject) => {
        let count = 1;
        mqttClient.on('message', function (topic, message) {
            try {
                const receivedMessage = JSON.parse(message.toString());

                expect(topic).toEqual(expectedTopic);
                expect(receivedMessage).toEqual(expectedMessage);

                count = count + 1;
                if (maxCount === 1 || count === maxCount) {
                    resolve();
                }
            }
            catch (err) {
                reject(err);
            }
        });
    });
}



/**
 * Dipose resources, disconnecting mqttClient and killing child process with SIGTERM
 * @param {*} mqttClient - mqttClient connection handle
 * @param {*} child - forked notify.js process 
 */
async function dispose(mqttClient, child) {
    if (mqttClient) {
        await mqttClient.end();
    }
    if (child) {
        child.kill('SIGTERM');
    }
}


/**
 * Load person detection object from JSON file asynchronously
 * @returns {Object} - Message 
 */
async function loadMessage(filePath) {
    return JSON.parse(await fs.readFile(filePath));
}


/**
 * Promise that resolves when total number of seconds has elapsed
 * @param {number} seconds - Number of seconds to wait
 */
async function wait(seconds) {
    return new Promise((resolve) => {
        setTimeout(resolve, seconds * 1000);
    });
}



module.exports = {
    countMessageReceivedAsync: countMessageReceivedAsync,
    dispose: dispose,
    loadMessage: loadMessage,
    wait: wait
}