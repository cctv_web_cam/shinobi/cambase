'use strict'


const mqttClient = require('../libs/utils/src/lib/mqttClient');


test('MQTT client created with valid url and ca certificate', () => {

    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`;
    const client = mqttClient('mqtts://test.mosquitto.org:8883', caFile);

    expect(client).toBeDefined();
    expect(client.constructor.name).toEqual('MQTTClient');
});


test('MQTT client publish message', async () => {

    expect.assertions(1);

    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`;
    const client = mqttClient('mqtts://test.mosquitto.org:8883', caFile);

    await expect(client.publish('test/topic', 'a short test')).resolves.toBeUndefined();
});
