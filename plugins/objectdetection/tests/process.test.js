'use strict';


const fs = require('fs').promises;

const { forkChildProcess, Process } = require('../libs/utils/src');
const { Timer } = require('../libs/utils/src/lib/timer');


jest.mock('../libs/utils/src/lib/mqttClient', () => {
    return jest.fn().mockImplementation((url, caFilePath) => {
        return {
            publish: async (topic, message) => {
                return;
            }
        };
    });
});


async function loadConfig() {
    return JSON.parse(await fs.readFile(`${__dirname}/config/conf.json`));
}


describe('forkChildProcess tests', () => {

    test('Process is forked with valid configuration', async () => {
        let args = await loadConfig();
        args.mqtt.cafile = `${__dirname}/docker/certs/localCA.crt`;

        let child = undefined;
        let error = false;

        try {
            child = await forkChildProcess(args, './notify.js');
        }
        catch (error) {
            error = true;
        }

        expect(error).toBe(false);
        expect(child).toBeDefined();
        expect(child).toHaveProperty('pid');

        child.kill('SIGTERM');
        expect(child.killed).toBeTruthy();
    });

    test('Error is raised for missing mqtt property', async () => {
        await expect(forkChildProcess({}, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Arguments are missing the mqtt property'
            );
    });

    test('Error is raised for missing mqtt.mqttsUrl property', async () => {
        const args = {
            mqtt: {

            },
            objectDetection: {

            }
        }
        await expect(forkChildProcess(args, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Missing mqtt.mqttsUrl argument'
            );
    });

    test('Error is raised for missing mqtt.cafile property', async () => {
        const args = {
            mqtt: {
                mqttsUrl: 'mqtts://user:pass@locahost:8883'
            },
            objectDetection: {

            }
        }
        await expect(forkChildProcess(args, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Missing mqtt.cafile argument'
            );
    });

    test('Error is raised for missing mqtt.rejectUnauthorised property', async () => {
        const args = {
            mqtt: {
                mqttsUrl: 'mqtts://user:pass@locahost:8883',
                cafile: '/path/to/some/ca/file'
            },
            objectDetection: {

            }
        }
        await expect(forkChildProcess(args, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Missing mqtt.rejectUnauthorised argument'
            );
    });

    test('Error is raised for missing objectDetection property', async () => {
        const args = {
            mqtt: {
                mqttsUrl: 'mqtts://user:pass@locahost:8883',
                cafile: '/path/to/some/ca/file',
                rejectUnauthorised: true
            }
        }
        await expect(forkChildProcess(args, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Arguments are missing the object detection property'
            );
    });

    test('Error is raised for missing objectDetection.timeoutPeriod property', async () => {
        const args = {
            mqtt: {
                mqttsUrl: 'mqtts://user:pass@locahost:8883',
                cafile: '/path/to/some/ca/file',
                rejectUnauthorised: true
            },
            objectDetection: {
            }
        }
        await expect(forkChildProcess(args, './notify.js'))
            .rejects
            .toHaveProperty(
                'message',
                'Missing objectDetection.timeoutPeriod argument'
            );
    });
});

describe('Process class', () => {

    test('Process constructor initialises timer, ProcessArgs and mqttClient', () => {

        const expectedCaFile = '/usr/local/share/ca-certificates/CA.crt';
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "true";
        const expectedTimeoutPeriod = 5000;

        const testProcess = new Process(['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod]);

        expect(testProcess).toBeDefined();
        expect(testProcess.constructor.name).toEqual('Process');
        expect(testProcess.args).toBeDefined();
        expect(testProcess.args.constructor.name).toEqual('ProcessArgs');
        expect(testProcess.args.args).toEqual(['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod]);
        expect(testProcess.args.mqttsUrl).toEqual('');
        expect(testProcess.args.cafile).toEqual('');
        expect(testProcess.args.timeoutPeriod).toEqual(0);
        expect(testProcess.args.rejectUnauthorised).toEqual(true);
        expect(testProcess.timers).toEqual({});
        expect(testProcess.mqttClient).toBeUndefined();
    });

    test('Process init method initialises arguments and mqttClient connection', async () => {
        const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`;
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "false";
        const expectedTimeoutPeriod = 5000;

        const expectedArgs = [
            'node',
            'notify.js',
            expectedMqttsUrl,
            expectedCaFile,
            expectedRejectUnauthorised,
            expectedTimeoutPeriod,
        ];

        const testProcess = new Process(expectedArgs);

        const argsMock = jest.spyOn(testProcess.args, 'init');
        
        await testProcess.init();
        
        expect(argsMock).toHaveBeenCalled();
        expect(testProcess.args.args).toEqual(expectedArgs);
        expect(testProcess.args.mqttsUrl).toEqual(expectedMqttsUrl);
        expect(testProcess.args.cafile).toEqual(expectedCaFile);
        expect(testProcess.args.timeoutPeriod).toEqual(expectedTimeoutPeriod);
        expect(testProcess.args.rejectUnauthorised).toEqual((expectedRejectUnauthorised==false));
        expect(testProcess.timers).toEqual({});
        expect(testProcess.mqttClient).toBeDefined();
    });


    test('Process message handler sends a message notification for a camera monitor not yet logged with a timer', async () => {
        
        const args = [
            'node',
            'notify.js',
            'mqtts://user:pass@localhost:8883',
            `${__dirname}/docker/certs/localCA.crt`,
            "false",
            5000,
        ];

        const data = { group: 'group', monitorId: 'monitorId' };
        const key = data.group + data.monitorId;

        const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`;

        const testProcess = new Process(args);
        await testProcess.init();

        const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish');
        await testProcess.messageHandler(data);

        expect(testProcess.timers[key]).toBeDefined();
        expect(testProcess.timers[key].running).toBeTruthy();
        
        expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data));
    });


    test('Process message handler sends a message notification for a camera monitor that has a timer but it is not running', async () => {
        
        const args = [
            'node',
            'notify.js',
            'mqtts://user:pass@localhost:8883',
            `${__dirname}/docker/certs/localCA.crt`,
            "false",
            5000,
        ];

        const data = { group: 'notRunningGroup', monitorId: 'notRunningMonitorId' };
        const key = data.group + data.monitorId;

        const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`;

        const testProcess = new Process(args);
        testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000);
        
        await testProcess.init();

        const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish');
        await testProcess.messageHandler(data);

        expect(testProcess.timers[key].running).toBeTruthy();
        expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data));
    });

    test('Process message handler discards message notification for a camera monitor that has a timer running', async () => {
        const args = [
            'node',
            'notify.js',
            'mqtts://user:pass@localhost:8883',
            `${__dirname}/docker/certs/localCA.crt`,
            "false",
            5000,
        ];

        const data = { group: 'runningGroup', monitorId: 'runningMonitorId' };
        const key = data.group + data.monitorId;

        const testProcess = new Process(args);
        testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000);
        
        await testProcess.init();

        testProcess.timers[key].run();

        const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish');
        await testProcess.messageHandler(data);

        expect(mqttPublishMock).not.toHaveBeenCalled();
        expect(testProcess.timers[key].running).toBeTruthy();
    });

    test('Process dispose method cancels running timer instances', async () => {
        const args = [
            'node',
            'notify.js',
            'mqtts://user:pass@localhost:8883',
            `${__dirname}/docker/certs/localCA.crt`,
            "false",
            5000,
        ];

        const data = { group: 'runningGroup', monitorId: 'runningMonitorId' };
        const key = data.group + data.monitorId;

        const testProcess = new Process(args);
        testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000);
        
        await testProcess.init();

        testProcess.timers[key].run();
        testProcess.dispose();
        
        expect(testProcess.timers[key].running).toEqual(false);
    });
});
