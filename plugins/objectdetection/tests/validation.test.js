'use strict';


const { ValidateProcess } = require('../libs/utils/src/lib/validation');


describe('validation tests', () => {
    test('Constructor initialises', () => {

        const expectedCaFile = '/usr/local/share/ca-certificates/CA.crt';
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "true";
        const expectedTimeoutPeriod = 5000;

        const argv = ['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod];

        const validate = new ValidateProcess(argv);


        expect(validate).toBeDefined();
        expect(validate.constructor.name).toEqual('ValidateProcess');
        expect(validate.args).toEqual(argv);
    });


    test('initArgs validates and returns an object representation of arguments', async () => {

        const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`;
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "true";
        const expectedTimeoutPeriod = 5000;

        const argv = ['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod];

        const validate = new ValidateProcess(argv);

        const args = await validate.initArgs();


        expect(args).toEqual({
            "cafile": `${__dirname}/docker/certs/localCA.crt`,
            "mqttsUrl": "mqtts://user:pass@localhost:8883",
            "rejectUnauthorised": true,
            "timeoutPeriod": 5000
        });

    });

    test('initArgs throws an error for invalid number of arguments', async () => {

        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const argv = ['node', 'notify.js', expectedMqttsUrl];
        
        const validate = new ValidateProcess(argv);

        await expect(validate.initArgs()).rejects.toThrow();
    });


    test('initArgs throws an error when timeout is not numeric', async () => {

        const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`;
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "true";
        const expectedTimeoutPeriod = 'moose';

        const argv = ['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod];
        const validate = new ValidateProcess(argv);

        await expect(validate.initArgs()).rejects.toThrow();
    });


    test('initArgs throws an error when rejectUnauthorised is not false or true', async () => {
        
        const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`;
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "test";
        const expectedTimeoutPeriod = '5000';

        const argv = ['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod];
        const validate = new ValidateProcess(argv);

        await expect(validate.initArgs()).rejects.toThrow();
    });


    test('initArgs throws an error when fails to locate CA file', async () => {

        const expectedCaFile = `/usr/local/share/ca-certificates/CA.crt`;
        const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883';
        const expectedRejectUnauthorised = "true";
        const expectedTimeoutPeriod = '5000';

        const argv = ['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod];
        const validate = new ValidateProcess(argv);

        await expect(validate.initArgs()).rejects.toThrow();
    });
});