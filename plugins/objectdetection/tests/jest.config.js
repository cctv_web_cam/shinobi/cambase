'use strict';


// jest.config.js
module.exports = {
    modulePathIgnorePatterns: ['./tests/e2e'],
    rootDir: `${__dirname}`,
    setupFiles: ['./setup/setupFiles.js'],
    verbose: true
};