'use strict';

const mqtt = require('async-mqtt');
const mqttClient = require('../../../libs/utils/src/lib/mqttClient');


describe('mqttClientTests', () => {
    test('publish to mqtt client', async (done) => {

        expect.assertions(2);

        const certificate = `${__dirname}/../../docker/certs/localCA.crt`;
        const mqttsUrl = 'mqtts://user:pass@localhost:8883';
        const testMessage = 'test message';
        const testTopic = 'test topic';

        const client = mqttClient(mqttsUrl, certificate, false);

        const mqttjsClient = await mqtt.connectAsync(mqttsUrl, { rejectUnauthorized: false });
        await mqttjsClient.subscribe(testTopic);

        mqttjsClient.on('message', async (topic, message) => {
            const receivedMessage = message.toString('utf8');

            expect(topic).toEqual(testTopic);
            expect(receivedMessage).toEqual(testMessage);

            try {
                await mqttjsClient.end();
            } catch (error) {
                throw error;
            }
            finally {
                done();
            }
        });
        
        await client.publish(testTopic, testMessage);
        
    });

    test('publish fails for an incorrect server url', async () => {
        const certificate = `${__dirname}/../../docker/certs/localCA.crt`;
        const mqttsUrl = 'mqtts://user:pass@badhost:8883';
        const testMessage = 'test message';
        const testTopic = 'test topic';

        const client = mqttClient(mqttsUrl, certificate, false, 4000);

        await expect(client.publish(testTopic, testMessage)).rejects.toThrow();
    });
});
