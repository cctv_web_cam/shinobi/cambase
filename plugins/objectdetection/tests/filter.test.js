'use strict';


test('filter object created', () => {
    const filterObj = require('../filter')(0.5, 'class1,class2,class3');

    expect(filterObj).toBeDefined();
    expect(filterObj.constructor.name).toEqual('FilterDetection');
});

test('filter condition passes above threshold', () => {
    const filterObj = require('../filter')(0.5, 'class1,class2,class3');

    const result = filterObj.filter('class1', 0.6);

    expect(result).toBe(true);
});

test('filter condition passes on threshold', () => {
    const filterObj = require('../filter')(0.5, 'class1,class2,class3');

    const result = filterObj.filter('class1', 0.5);

    expect(result).toBe(true);
});


test('filter condition fails below threshold', () => {
    const filterObj = require('../filter')(0.5, 'class1,class2,class3');

    const result = filterObj.filter('class1', 0.4);
    
    expect(result).toBe(false);
});

test('filter condition fails due to unrecognised prediction class', () => {
    const filterObj = require('../filter')(0.5, 'class1,class2,class3');

    const result = filterObj.filter('class4', 0.5);
    
    expect(result).toBe(false);
});

