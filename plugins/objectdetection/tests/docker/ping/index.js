/**
 * Load modules
 */
const http = require('http');
const mqtt = require('async-mqtt');


/**
 * Local modules
 */
const config = require('./config');


/**
 * Constants
 */
const HOST = '0.0.0.0';
const PORT = 8080;



/**
 * Listen for ping requests on http://${host}:${port}/ping
 * @param {*} req - Http request 
 * @param {*} res - Http response
 * @returns {number} - 200 if able to ping mqtt server, otherwise 404 for failed ping op.
 */
const requestListener = async function (req, res) {
    res.setHeader("Content-Type", "application/json");

    /** Determine the route */
    switch (req.url) {
        case "/ping":
            const success = await connectToMqttServer(`${config.mqttUrl}`);
            if(success) {
                res.writeHead(200, 'OK');
                res.write('Connection OK')
            }
            else {
                res.writeHead(404, 'Failed connection');
                res.write('Failed to connect to mqtt broker');
            }
            res.end();
            break;
        default:
            res.writeHead(404);
            res.end(`send ping requests to http://${HOST}:${PORT}/ping`);
    }
};



/**
 * Try and connectt to mqtt server on a specific host and port
 * @param {string} url - mqtt url that is forwarded to underlying mqttjs library 
 * @returns {boolean} - true if connection is successful, otherwise false
 */
async function connectToMqttServer(url) {
    
    let client = undefined;
    const options = {};

    try {
        console.log(`Connecting to ${url}...`);
        client = await mqtt.connectAsync(url, options, false);
        console.log(`Connected!`);
    }
    catch(error) {
        console.error(`Error ${error} encountered connecting to ${url}`);
        return false;
    }
    finally{ 
        if(client && client.connected) {
            await client.end();
            console.log('Connection closed');
            return true;
        }
        else {
            return false;
        }
    }
}



/**
 * Create the server
 */
const server = http.createServer(requestListener);
let listener = server.listen(PORT, HOST, () => {
    console.log(`Server is running on http://${HOST}:${PORT}`);
    console.log(`Try and ping the mqtt server at http://${HOST}:${PORT}/ping`);
});


/**
 * Graceful shutdown of server
 */
let lastSocketKey = 0;
let socketMap = {};

/**
 * Signal handlers 
 */
process.on('SIGINT', end);
process.on('SIGTERM', end);


listener.on('connection', function(socket) {
    /* generate a new, unique socket-key */
    const socketKey = ++lastSocketKey;
    /* add socket when it is connected */
    socketMap[socketKey] = socket;
    socket.on('close', function() {
        /* remove socket when it is closed */
        delete socketMap[socketKey];
    });
});

function end() {
    console.log('Preparing process for graceful exit');

    /* loop through all sockets and destroy them */
    Object.keys(socketMap).forEach(function(socketKey){
        socketMap[socketKey].destroy();
    });

    /* after all the sockets are destroyed, we may close the server! */
    listener.close(function(err){
        if(err) throw err();

        console.log('Server stopped');
        /* exit gracefully */
        process.exit(0);
    });
}
