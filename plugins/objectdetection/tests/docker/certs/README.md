## Overview

Use this directory to place server certificate and certificate authority files for testing
This should contain the following files:

- localCA.crt
- server.crt
- server.key

The server certificate is for use in a docker container:

CN: localhost
SANs:
- DNS:localhost, DNS:127.0.0.1, DNS:::1, DNS:docker.for.mac.localhost, DNS:docker.for.windows.localhost, DNS:docker.for.linux.localhost

