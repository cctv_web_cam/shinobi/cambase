## Overview

Docker compose architecture that provides the following services intended for use in development environments for testing:

- A Mosquitto MQTT broker for running on localhost ports: 1883 and 8883. Also serves    
  websocket requests on port 9001.
- A NodeJs http server for connecting to the Mosquitto broker instance @ 
  http://localhost:8080/ping. Returns 200 if successful, otherwise returns 404.
  This is useful to provide start-server-and-test functionality for end-to-end tests

Provide the following files in the _certs_ folder:

- *localCA.crt*: Certificate Authority root certificate
- *server.crt*: Server certificate for the broker
- *server.key*: Server certificate key

Provide *conf.json* file in _tests/config_ folder. A _sample.conf.json_ exists in
_tests/config_.

The above settings are unique for the developers environment and allows an MQTT broker to be 
quickly started for each test run, once configured
