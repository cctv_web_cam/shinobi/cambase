const fs = require('fs').promises;


/** 
 * Check if a file exists
 * @param {string} - The file path
 * @returns {boolena} - True if the file exists, otherwise false
 */
async function fileExistsAsync(path) {
    let exists;

    try {
        await fs.readFile(path);
        exists = true;
    }
    catch (err) {
        exists = false;
    }

    return exists;
}


/**
 * Return the file exists status for localCA, server.crt and server.key
 */
async function getCertStatus() {

    const caPath = `${__dirname}/../docker/certs/localCA.crt`;
    const serverCertPath = `${__dirname}/../docker/certs/server.crt`;
    const serverKeyPath = `${__dirname}/../docker/certs/server.key`;

    caExists = await fileExistsAsync(caPath);
    serverCertExists = await fileExistsAsync(serverCertPath);
    serverKeyExists = await fileExistsAsync(serverKeyPath);

    return {
        caExists: caExists,
        serverCertExists: serverCertExists,
        serverKeyExists: serverKeyExists,
    };
};


module.exports = async () => {
    const status = await getCertStatus();

    return status;
}
