const detail = require('./utils');


detail()
    .then((info) => {
        if (!info.caExists || !info.serverCertExists || !info.serverKeyExists) {
            throw new Error('3 certificates should exist in <root dir>/tests/docker/certs :: localCA.crt, server.crt and server.key');
        }
    })
    .catch((error) => { throw error });
