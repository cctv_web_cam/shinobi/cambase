'use strict';


/**
 * Module that exports a FilterDetection instance to filter object predictions with:
 * - confidence rating >= minimum score
 * - classes that exists within a comma separated list of classes
 * @module filter
 * @author Simon Pears
 */


/** @class Class to filter predicted object detected classes and confidence rating */
class FilterDetection {



    /**
     * Create an instance of the filter.
     * 
     * @constructor
     * @param {number} minConfidence - Decimal minimum confidence rating for a prediction to be filtered.
     * @param {string} detectionClasses - A comma separated list of object classes recognised by the filer. 
     */
    constructor(minConfidence, detectionClasses) {
        this.minConfidence = minConfidence;
        this.setClasses = new Set(detectionClasses.split(','));
    }


    /**
     * Returns true if:
     * - score >= minimum confidence
     * - predictedClass exists in classes dictionary
     * 
     * @param {string} predictedClass - Predicted class 
     * @param {double} score - Confidence score for predicted class 
     * 
     * @returns {boolean} - true if filter matched, otherwise false  
     */
    filter(predictedClass, score) {
        return (score >= this.minConfidence && this.setClasses.has(predictedClass)) ? true : false;
    }
}


/** 
 * Factory method that create a class that filters object predictions that satisfy a minimum confidence 
 * rating and recognised object detection classes.
 * @param {number} minConfidence - Minimum confidence rating score to satisfy filter
 * @param {string} recogniseClasses - Comma delimited list of classes recognised by filter 
 * 
 * @returns {FilterDetection} - Instance of filter detection class
 */
module.exports = (minConfidence, recogniseClasses) => {

    return new FilterDetection(minConfidence, recogniseClasses);

}