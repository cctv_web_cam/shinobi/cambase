# Shinobi MQTT Tensorflow Plugin for compatible with Intel Core 2 processes

This is a fork of [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) that includes a derivation of the 
tensorflow objectdetection plugin compatible with Intel Core 2 processors on Linux Ubuntu 20.04 with 
tfjs-node@1.7.3 installed. Additionally, I am expanding the plugin using so that it can be configured to filter 
detections above a specific confidence threshold that belong to a set of given object classes. Filtered
detections are dispatched onto an MQTT queue including the image that the triggered the detection.
This aspect is work in process and gives me an opportunity to  develop with MQTT. Shinobi already provides a 
GUI for specifying filters for object classes and confidence thresholds. However, to allow access to the image 
that triggered the detection I delved into the source code.

The plugin is available at this [repository](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin.git) 
for cloning into the plugins folder of an existing Shinobi installation.


## Why?

It includes a plugin, *plugins/objectdetection*, that is based on the Shinobi tensorflow plugin with the 
following updates:

- INSTALL.sh prompts for use of Intel Core2 CPU. If this CPU type is present on the machine then a precompiled
  compatible libtensorflow-CPU library will be extracted into *node_modules/@tensorflow/tfjs-node/deps/*
  The library was compiled on Linux Ubuntu Server 16.04 and targeted core2 processors as the machine
  architecture.
- Avoids an incompatibility issue with tfjs-node whereby the default precompiled libtensorflow libraries and 
  headers provided cause the Shinobi-Tensorflow plugin to continuously restart. Please refer to this 
  [link](https://gitlab.com/dcs3spp/myfiles/-/blob/master/COMPILING_LIBTENSORFLOW.md) for further background 
  information and details on how to compile libtensorflow library from source if experiencing issues with the 
  Shinobi tensorflow plugin restarting on architectures other than Ubuntu Linux. 
- INSTALL.sh still prompts user if wish to install @tensorflow/tfjs-node-gpu. However, Yarn is used to install 
  the plugin. A package.json file facilitates management of tensorflow dependencies. @tensorflow/tfjs-node-gpu 
  is included as an optional dependency in package.json. Installation of @tensorflow/tfjs-node-gpu is now
  bypassed by using yarn --ignore-optional, i.e. when the user elects not to install the gpu module by choosing 
  N when prompted.
- Work in progress for allowing image(s) for filtered detections to be dispatched to a specified MQTT broker.
  Detections can be filtered with a minimum confidence threshold and set of object classes.

For personal use, ease of installation and greater control, I have also tweaked the ubuntu install shell scripts to:

- Install pm2 v4.4.0. This avoids the message ```DeprecationWarning OutgoingMessage.prototype._headers is deprecated```
  in the logs. 
- Do not symbolic link ~/shinobi/shinobi => /bin/shinobi. Installation on my development environment is located in a 
  different folder. May replace it in future to /usr/local/bin.... 
- Soon will be using *engines* option in package.json to enforce compatible nodejs engines for main
  installation and yarn version for the plugin. This will be restricted to nodejs versions between 10.19.0 and 12.16.3.

For further details please refer to the README.md file in _plugins/objectdetection_ folder.

# Links

Documentation - http://shinobi.video/docs

Donate - https://shinobi.video/docs/donate

Tested Cameras and Systems - http://shinobi.video/docs/supported

Features - http://shinobi.video/features

Reddit (Forum) - https://www.reddit.com/r/ShinobiCCTV/

YouTube (Tutorials) - https://www.youtube.com/channel/UCbgbBLTK-koTyjOmOxA9msQ

Discord (Community Chat) - https://discordapp.com/invite/mdhmvuH

Twitter (News) - https://twitter.com/ShinobiCCTV

Facebook (News) - https://www.facebook.com/Shinobi-1223193167773738/?ref=bookmarks


